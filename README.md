
## Plataforma 2
El proyecto utiliza:
* Docker 18.09.7
* Docker-compose 1.17.1
* Php 7.3
* MySql 8.0

## Instalación
### Instalar Docker
```bash
sudo apt-get update
```
```bash
sudo apt install docker.io
```
```bash
sudo systemctl start Docker
```
```bash
sudo systemctl enable Docker
```
```bash
docker –-version
```
### Instalar Docker-compose
```bash
sudo apt install docker-compose
```
```bash
sudo chmod +x /usr/bin/docker-compose
```
```bash
docker-compose --version
```
```bash
sudo usermod -a -G docker $USER
```

## Descargar proyecto
```bash
git clone https://gitlab.com/oscarin.molina88/tarea-app-helpdesk.git
```

## Crear y correr la imagen
Dentro de la carpeta descargada *tarea-app-helpdesk* se debe correr el comando:
```bash
sudo docker-compose up --build
```
## Ingresar a la aplicación
La aplicación se encuentra configurada para correr por el puerto **8080**

